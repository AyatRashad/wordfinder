# -*- coding: utf-8 -*-

from goose import Goose
from goose.text import StopWordsArabic

from collections import defaultdict
import logging

def setup_log(name):
    log = logging.getLogger(name)
    log.setLevel(logging.DEBUG)

    fh = logging.FileHandler('log/%s.log' %name)
    fh.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    log.addHandler(fh)

    return log


_log = setup_log('wordfinder')


class WordFinder:
    def __init__(self):
        self._g = Goose({'stopwords_class': StopWordsArabic})

    def find_words(self, data):
        """
        parameters:
            data: {
            html: (str) html of the page,
            words: (list) the words to find
            }
            
        returns:
            if failure: {error: "error message"},
            if success:
            result: {
            article_body: (str) the xpath of the main part of the page,
            for every word received in the request:
                word: (list) the xpaths of the elements where this word exist.
            }
        """
        words_loc = defaultdict(list)

        try:
            words, html = data['words'], data['html']
        except Exception as e:
            msg = 'wrong data structure, make sure you included fields "words" and "html".'
            _log.error(msg)
            return -1, {'error': msg}

        try:
            article = self._g.extract(raw_html=html)
        except Exception as e:
            msg = '%s: %s' %(type(e), e)
            _log.error(msg)
            return -1, {'error': msg}

        node = article.top_node
        tree = node.getroottree()
        article_path = tree.getelementpath(node)

        words_loc['article_body'] = tree.getelementpath(node)

        for word in words:
            locations = node.xpath(u"//%s/*[text()[contains(.,'%s')]]" %(article_path,word))
            paths = [tree.getelementpath(loc) for loc in locations]
            words_loc[word].extend(paths)

        return 0, words_loc
