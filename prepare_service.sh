apt-get install libxml2-dev libxslt1-dev libxml2 lib32z1-dev

virtualenv wfinder-env
source wfinder-env/bin/activate

git clone https://github.com/grangier/python-goose.git
cd python-goose
pip install lxml
pip install -r requirements.txt
python setup.py install

deactivate

uwsgi --ini uwsgi.ini --pidfile /tmp/wf-serv-master1.pid &
