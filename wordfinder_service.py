from word_finder import WordFinder, setup_log

import json
import zlib

import tornado.ioloop
import tornado.web
from tornado import wsgi


_log = setup_log('wf_service')


class MainHandler(tornado.web.RequestHandler):
    def prepare(self):
        if self.request.headers["Content-Type"].startswith("application/json"):
            if self.request.headers["Content-Encoding"].startswith("gzip"):
                data = zlib.decompress(self.request.body)
            else:
                data = self.request.body

            try:
                self.json_args = json.loads(data)
            except Exception as e:
                _log.error('wrong data format, make sure the json is well formatted.')
                self.json_args = None
        else:
            self.json_args = None


class WordHandler(MainHandler):
    def post(self):
        if self.json_args == None:
            _log.error('could not find request data, make sure it is json.')
            self.set_status(500)
            self.write({'error': 'could not find request data, make sure it is json.'})
            return

        req_data = self.json_args
        wf = WordFinder()
        code, data = wf.find_words(req_data)

        if code != 0:
            self.set_status(500)
            self.write(data)
        else:
            response_data = zlib.compress(json.dumps(data))

            self.set_status(200)
            self.set_header("Content-Type", "application/json")
            self.set_header("Content-Encoding", "gzip")
            self.write(response_data)


# Application and its wsgi interface
application = tornado.web.Application([
    (r"/words", WordHandler)
])
wsgi_app = wsgi.WSGIAdapter(application)
