__/words:__  
    parameters: __json__ (gzipped)  
            `data`: {  
            `html`: __(str)__ html of the page,  
            `words`: __(list)__  (list) the words to find  
}
    
  
returns: __json__   
            if success:  __(gzip)__  
{  
`article_body`: __(str)__ the xpath of the main part of the page,  
  
for every word __w__ received in the request:  
`w`: __(list)__ the xpaths of the elements where this word exist.  
}  
if failure:  
 { `error`: "message" }
               